package dv.spring.demo.repository

import dv.spring.demo.entity.SelectedProduct
import org.springframework.data.repository.CrudRepository

interface SelectedProductRepository:CrudRepository<SelectedProduct,Long>