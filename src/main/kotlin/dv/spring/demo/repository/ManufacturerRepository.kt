package dv.spring.demo.repository

import dv.spring.demo.entity.Manufacturer
import org.springframework.data.repository.CrudRepository

interface ManufacturerRepository:CrudRepository<Manufacturer,Long>