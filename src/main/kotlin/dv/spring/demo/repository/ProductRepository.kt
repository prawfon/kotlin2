package dv.spring.demo.repository

import dv.spring.demo.entity.Product
import org.springframework.data.repository.CrudRepository

interface  ProductRepository:CrudRepository<Product,Long>