package dv.spring.demo.repository


import dv.spring.demo.entity.Customer
import org.springframework.data.repository.CrudRepository

interface CustomerRepository:CrudRepository<Customer,Long>