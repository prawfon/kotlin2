package dv.spring.demo.repository


import dv.spring.demo.entity.Address
import org.springframework.data.repository.CrudRepository

interface  AddressRepository:CrudRepository<Address,Long>