package dv.spring.demo.util



import dv.spring.demo.entity.*
import dv.spring.demo.entity.dto.*
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface  MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    @Mappings(
            Mapping(source = "manufacturer",target = "manu")
    )
    fun mapProductDto(product: Product):ProductDto
    fun mapProductDto(product: List<Product>):List<ProductDto>
    fun mapManufacturer(manu: Manufacturer):ManufacturerDto

    fun mapCustomerDto(customer: Customer): CustomerDto
    fun mapCustomerDto(customer: List<Customer>): List<CustomerDto>

    fun mapShoppingCart(shoppingCart: ShoppingCart):ShoppingCartDto
    fun mapShoppingCart(shoppingCart:List<ShoppingCart>):List<ShoppingCartDto>

    fun mapSelectedProduct(selectedProduct: SelectedProduct):SelectedProductDto
    fun mapSelectedProduct(selectedProduct: List<SelectedProduct>): List<SelectedProductDto>


}