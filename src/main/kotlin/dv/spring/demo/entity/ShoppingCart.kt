package dv.spring.demo.entity

import javax.persistence.*

@Entity
data class ShoppingCart (var shoppingCartStatus: ShoppingCartStatus ){
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany
    var selectedProduct = mutableListOf<SelectedProduct>()
    @OneToOne
    var shippingAddress: Address? =null
//    lateinit var shippingAddress: Address
    @ManyToOne
    var customer: Customer? = null
//    lateinit var customer: Customer
}


