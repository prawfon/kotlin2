package dv.spring.demo.entity.dto

import dv.spring.demo.entity.Address
import dv.spring.demo.entity.User
import dv.spring.demo.entity.UserStatus
import javax.persistence.ManyToOne

data class CustomerDto ( override var name: String? = null,
                         override var email: String? = null,
                         override var userStatus: UserStatus? = UserStatus.PENDING,
                         var shippingAddress: List<Address>?= mutableListOf<Address>(),
                         var billingAddress: Address? = null,
                         var defaultAddress: Address? = null
) : User