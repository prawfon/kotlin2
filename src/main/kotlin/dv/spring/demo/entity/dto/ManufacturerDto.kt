package dv.spring.demo.entity.dto

data class ManufacturerDto(
        var name:String?=null,
        var telNo:String? = null
)