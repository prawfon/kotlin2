package dv.spring.demo.entity.dto

import dv.spring.demo.entity.Product

data class SelectedProductDto(
        var quantity: Int?=null,
        var product: ProductDto?=null

)
