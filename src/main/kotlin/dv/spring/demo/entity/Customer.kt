package dv.spring.demo.entity

import javax.persistence.*

@Entity
data class Customer(
        override var name: String? = null,
        override var email: String? = null,
        override var userStatus: UserStatus? = UserStatus.PENDING
) : User {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToMany
    var shippingAddress = mutableListOf<Address>()
    @ManyToOne
    var billingAddress:Address? = null
//    lateinit var billingAddress: Address
    @ManyToOne
    var defaultAddress: Address? = null
//    lateinit var defaultAddress: Address
}

