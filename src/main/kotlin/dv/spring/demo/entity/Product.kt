package dv.spring.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class  Product (var name:String,
                     var description:String,
                     var price:Double,
                     var amountInStock:Int,
                     var imageUrl:String?){
    @Id
    @GeneratedValue
    var id:Long? = null
    @ManyToOne
    var manufacturer: Manufacturer?=null
    constructor(
            name:String,
            description: String,
            price: Double,
            amountInStock: Int,
            manufacturer: Manufacturer,
            imgeUrl: String?):
            this(name, description, price, amountInStock, imgeUrl){
        this.manufacturer=manufacturer
    }
}
