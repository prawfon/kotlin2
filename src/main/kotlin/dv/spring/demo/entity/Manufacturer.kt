package dv.spring.demo.entity

import javax.persistence.*

@Entity
data class Manufacturer(var name:String,
                        var telNo:String){
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany(mappedBy = "manufacturer")
    var products = mutableListOf<Product>()
    @OneToOne
//    var address:Address?=null
    lateinit var address:Address
}