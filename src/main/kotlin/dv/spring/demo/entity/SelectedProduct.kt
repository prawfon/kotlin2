package dv.spring.demo.entity

import javax.persistence.*

@Entity
data class SelectedProduct(var quantity: Int) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToOne
    lateinit var product: Product
//    var products = mutableListOf<Product>()

}
