package dv.spring.demo.entity


interface User {
    var name: String?
    var email: String?
    var userStatus: UserStatus?
}