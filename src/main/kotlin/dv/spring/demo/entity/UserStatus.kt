package dv.spring.demo.entity

enum class UserStatus{
    PENDING,ACTIVE,NOTACTIVE,DELETED
}