package dv.spring.demo.config

import dv.spring.demo.entity.*
import dv.spring.demo.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var selecteProductRepository: SelectedProductRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    @Transactional
    override fun run(args: ApplicationArguments) {
        var manu1 = manufacturerRepository.save(Manufacturer("Apple", "053123456"))
        var manu2 = manufacturerRepository.save(Manufacturer("Sumsung", "555666777888"))
        var manu3 = manufacturerRepository.save(Manufacturer("CAMT", "0000000"))

        var product1 = productRepository.save(Product(
                "iPhone",
                "It's a phone",
                28000.00,20,
//                Manufacturer("Apple","053123456"),
                "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        var product2 = productRepository.save(Product(
                "Note 9",
                "Other Iphone",28001.00,
                10,
//                Manufacturer("Samsung","555666777888"),
                "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        var product3 = productRepository.save( Product(
                "CAMT",
                "The best College in CMU",
                0.00,
                1,
//                Manufacturer("CAMT","0000000"),
                "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        var product4 = productRepository.save(Product(
                "Prayuth",
                "The best PM ever",
                1.00,1,
//                Manufacturer("CAMT","0000000"),
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))

        manu1.products.add(product1)
        product1.manufacturer = manu1

        manu2.products.add(product2)
        product2.manufacturer = manu2

        manu3.products.add(product3)
        product3.manufacturer = manu3

        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย","แขวง ินสอ เขตดุสิต","กรุงเทพ","10123"))
        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่","เมือง","เชียงใหม่","50200"))
        var address3 = addressRepository.save(Address("ซักที่บนโลก","สุขสันต์","ขอนแก่น","12457"))

        var customer1 = customerRepository.save(Customer("Lung","pm@go.th",UserStatus.ACTIVE))
        var customer2 = customerRepository.save(Customer("ชัชชาติ","chut@taopoon.com",UserStatus.ACTIVE))
        var customer3 = customerRepository.save(Customer("ธนาธร","thanathorn@life.com",UserStatus.PENDING))

        customer1.defaultAddress= address1
        customer2.defaultAddress= address2
        customer3.defaultAddress= address3


        var quantity1 = selecteProductRepository.save(SelectedProduct(4))
        quantity1.product=product1
        var quantity2 = selecteProductRepository.save(SelectedProduct(1))
        quantity2.product=product4
        var quantity3 = selecteProductRepository.save(SelectedProduct(1))
        quantity3.product=product4
        var quantity4 = selecteProductRepository.save(SelectedProduct(1))
        quantity4.product=product3
        var quantity5 = selecteProductRepository.save(SelectedProduct(2))
        quantity5.product=product2




        var shoppingCart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        var shoppingCart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))

        shoppingCart1.customer=customer1
        shoppingCart1.selectedProduct.add(quantity1)
        shoppingCart1.selectedProduct.add(quantity2)
        shoppingCart2.customer=customer2
        shoppingCart1.selectedProduct.add(quantity3)
        shoppingCart1.selectedProduct.add(quantity4)
        shoppingCart1.selectedProduct.add(quantity5)
    }
}