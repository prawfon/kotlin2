package dv.spring.demo.service

import dv.spring.demo.dao.ProductDao
import dv.spring.demo.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProductServiceImpl : ProductService {
    @Autowired
    lateinit var productDao: ProductDao

    override fun getProducts(): List<Product> {
        return productDao.getProduct()
    }
}
