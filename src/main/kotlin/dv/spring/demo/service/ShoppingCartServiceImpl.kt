package dv.spring.demo.service


import dv.spring.demo.dao.ShoppingCartDao
import dv.spring.demo.entity.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl:ShoppingCartService {
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCart()
    }

}