package dv.spring.demo.service

import dv.spring.demo.entity.Product

interface  ProductService {
    fun getProducts(): List<Product>
}
