package dv.spring.demo.service

import dv.spring.demo.dao.ManufacturerDao
import dv.spring.demo.entity.Manufacturer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServiceImpl : ManufacturerService {
    @Autowired
    lateinit var manufacturerDao: ManufacturerDao

    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getMenufacturers()
    }
}