package dv.spring.demo.service

import dv.spring.demo.entity.Manufacturer

interface  ManufacturerService {
    fun getManufacturers(): List<Manufacturer>
}

