package dv.spring.demo.service

import dv.spring.demo.entity.Customer

interface CustomerService{
    fun getCustomers(): List<Customer>
}