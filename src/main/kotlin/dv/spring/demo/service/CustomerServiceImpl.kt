package dv.spring.demo.service

import dv.spring.demo.dao.CustomerDao
import dv.spring.demo.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerServiceImpl: CustomerService {
    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomer()
    }
}