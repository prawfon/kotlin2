package dv.spring.demo.service

import dv.spring.demo.entity.ShoppingCart

interface  ShoppingCartService {
    fun getShoppingCarts(): List<ShoppingCart>
}
