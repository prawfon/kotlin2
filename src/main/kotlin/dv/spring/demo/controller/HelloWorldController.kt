package dv.spring.demo.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import sun.security.x509.AccessDescription

@RestController
class HelloWorldController {
    //    @GetMapping("/helloWorld")
//    fun  getHelloWorld(): String {
//        return "HelloWorld"
//    }
//    @GetMapping("/test")
//    fun  gettest(): String {
//        return "CAMT"
//    }
//    @GetMapping("/person")
//    fun  getPerson(): ResponseEntity<Any> {
//        val person = Person("somchai","somrak",15)
//                return ResponseEntity.ok(person)
//    }
//    @GetMapping("/myPerson")
//    fun  getMyPerson(): ResponseEntity<Any> {
//        val person = Person("Praw","Fon",21)
//        return ResponseEntity.ok(person)
//    }
//    @GetMapping("/myPersons")
//    fun  getMyPersons(): ResponseEntity<Any> {
//        val person01 = Person("Osora","Tsubasa","Nachansu",10)
//        val person02 = Person("Hyuoka","Kojiro","Meiwa",9)
//
//        val persons = listOf<Person>(person01,person02)
//        return ResponseEntity.ok(persons)
//    }
//    @GetMapping("/params")
//    fun getParams(@RequestParam("name") name: String, @RequestParam("surname") surname: String)
//            : ResponseEntity<Any> {
//        return ResponseEntity.ok("$name $surname")
//    }
//    @GetMapping("/params/{name}/{surname}/{age}")
//    fun getPathParam(@PathVariable("name") name: String,
//                     @PathVariable("surname") surname: String,
//                     @PathVariable("age") age:Int)
//            : ResponseEntity<Any> {
//        val person = Person(name,surname,age)
//        return ResponseEntity.ok(person)
//    }
//    @PostMapping("/echo")
//    fun echo(@RequestBody person: Person) : ResponseEntity<Any>{
//        return  ResponseEntity.ok(person)
//
//    }
    @GetMapping("/getAppName")
    fun  getAppName(): String {
        return "assessment"
    }
    @GetMapping("/product")
    fun  getproduct(): ResponseEntity<Any> {
        val product = Product("iPhone","A new telephone",28000,5)
                return ResponseEntity.ok(product)
    }

//    @GetMapping("/product/{name}")
//    fun getPathProduct(@PathVariable("name") name: String): ResponseEntity<Any> {
//        val product = Product("iPhone","A new telephone",28000,5)
//        return ResponseEntity.ok(product)
//    }
    @GetMapping("/product/{name}")
    fun getPathProductNotfound(@PathVariable("name") name: String): ResponseEntity<Any> {
        val product = Product("iPhone","A new telephone",28000,5)
        if (product.name == name){
            return ResponseEntity.ok(product)
        }else
        return ResponseEntity.notFound().build()
    }
    @PostMapping("/setZeroQuantity")
    fun getZeroQuantity(@RequestBody product: Product): ResponseEntity<Any> {
        product.quantity = 0
            return ResponseEntity.ok(product)
    }
    @PostMapping("/totalPrice")
    fun gettotalPrice(@RequestBody product: Array<Product>): String {
        var total = 0
        for (value in product){

            total=total+value.price
        }
        return "$total"
    }

    @PostMapping("/avaliableProduct")
    fun getavaliableProduct(@RequestBody product: Array<Product>) : ResponseEntity<Any>{
        var list = mutableListOf<Product>()
        for (value in product){
            println(value)
            println(product)
            if(value.quantity!=0){
                list.add(value)
            }
        }
        return  ResponseEntity.ok(list)
    }

}
//data class Person(var name:String,var surname:String,var age:Int)
//data class Person(var name:String, var surname:String, var team:String, var number:Int)
data class Product(var name:String,var description:String,var price:Int,var quantity:Int)




