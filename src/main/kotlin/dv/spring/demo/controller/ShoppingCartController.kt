package dv.spring.demo.controller


import dv.spring.demo.service.ShoppingCartService
import dv.spring.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/ShoppingCart")
    fun getAllShoppingCart(): ResponseEntity<Any> {
        val shoppingCarts = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCart(shoppingCarts))
    }
}