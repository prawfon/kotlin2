package dv.spring.demo.controller

import dv.spring.demo.service.ProductService
import dv.spring.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class ProductController {
    @Autowired
    lateinit var productService: ProductService

    @GetMapping("/productss")
    fun getAllProduct(): ResponseEntity<Any> {
        val products = productService.getProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(products))
    }
}