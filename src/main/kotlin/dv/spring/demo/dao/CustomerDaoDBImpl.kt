package dv.spring.demo.dao

import dv.spring.demo.entity.Customer
import dv.spring.demo.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl: CustomerDao{
    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomer(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }
}
