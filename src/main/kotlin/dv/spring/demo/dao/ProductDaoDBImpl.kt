package dv.spring.demo.dao

import dv.spring.demo.entity.Product
import dv.spring.demo.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class ProductDaoDBImpl: ProductDao{
    @Autowired
    lateinit var productRepository: ProductRepository

    override fun getProduct(): List<Product> {
        return  productRepository.findAll().filterIsInstance(Product::class.java)
    }
}