package dv.spring.demo.dao

import dv.spring.demo.entity.Manufacturer
import org.springframework.stereotype.Repository

@Repository
class ManufacturerDaoImpl : ManufacturerDao {
    override fun getMenufacturers(): List<Manufacturer> {
        return mutableListOf(Manufacturer("Apple", "053123456"),
                Manufacturer("Sumsung", "555666777888"),
                Manufacturer("CAMT", "0000000"))
    }
}