package dv.spring.demo.dao

import dv.spring.demo.entity.Customer

interface CustomerDao{
    fun getCustomer():List<Customer>
}