package dv.spring.demo.dao

import dv.spring.demo.entity.Product

interface ProductDao{
    fun getProduct():List<Product>
}