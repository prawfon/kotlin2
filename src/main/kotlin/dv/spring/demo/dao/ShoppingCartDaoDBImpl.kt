package dv.spring.demo.dao


import dv.spring.demo.entity.ShoppingCart
import dv.spring.demo.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ShoppingCartDaoDBImpl:ShoppingCartDao{
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCart(): List<ShoppingCart> {
        return  shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}