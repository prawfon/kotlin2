package dv.spring.demo.dao

import dv.spring.demo.entity.Manufacturer

interface ManufacturerDao{
    fun getMenufacturers():List<Manufacturer>
}